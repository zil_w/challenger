FROM ubuntu:18.04
LABEL maintainer="Network Silence"

ENV TERM screen-256color
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get install -y \
	sudo

RUN adduser --disabled-password --gecos '' developer
RUN adduser developer sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER developer
RUN mkdir -p /home/developer
ENV HOME /home/developer
WORKDIR /home/developer


RUN apt-get update && apt-get install -y \
	apt-utils \
	software-properties-common \
	python3-dev \
	python3-pip \
	git \
	valgrind

RUN cd /opt && \
    git clone https://gitlab.com/SparrowOchon/neovim-setup.git && \
    ./neovim-setup/install.sh full

